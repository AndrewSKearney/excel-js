'use strict';

/**
 * @ngdoc service
 * @name graphMeApp.excel
 * @description
 * # excel
 * Service in the graphMeApp.
 */
angular.module('graphMeApp')
  .service('excel', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function

    this.getfile = function(file, callbk) {

        var url = file;
        var oReq = new XMLHttpRequest();
        oReq.open("GET", url, true);
        oReq.responseType = "arraybuffer";

        oReq.onload = function(e) {
            var arraybuffer = oReq.response;

            /* convert data to binary string */
            var data = new Uint8Array(arraybuffer);
            var arr = [];
            for(var i = 0; i !== data.length; ++i) {
                arr[i] = String.fromCharCode(data[i]);
            }
            var bstr = arr.join("");

            /* Call XLSX */
            var workbook = XLSX.read(bstr, {type:"binary"});

            callbk(workbook);

        };

        oReq.send();

    };

      this.tojson = function(sheet) {

          var data = XLSX.utils.sheet_to_json(sheet);

          return data;

      }

  });
