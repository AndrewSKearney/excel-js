'use strict';

/**
 * @ngdoc function
 * @name graphMeApp.controller:RepChartCtrl
 * @description
 * # RepChartCtrl
 * Controller of the graphMeApp
 */
angular.module('graphMeApp')
  .controller('RepChartCtrl', function ($scope, excel) {

      excel.getfile("excel/SampleData.xlsx", function(workbook) {

          var first_sheet_name = workbook.SheetNames[0];

          /* Get worksheet */
          var worksheet = workbook.Sheets[first_sheet_name];
          var data = excel.tojson(worksheet);

          // lets output some data labels

          // loop the data and gets the reps associated with the sales
          var indexes = {};
          $scope.labels = [];
          $scope.data = [[]];
          $scope.series = ['Series A'];

          for(var i=0;i<data.length;i++) {

              var rep = data[i].Rep.trim();

              if($scope.labels.indexOf(rep) == -1) {
                  // add to array
                  $scope.labels.push(rep);
                  indexes[rep] = $scope.labels.indexOf(rep);
                  $scope.data[0].push(0);
              }

          }

          for(var k in indexes) {

              for(var i=0;i<data.length;i++) {
                  if(data[i].Rep == k) {
                      $scope.data[0][indexes[k]] += Math.round(parseInt(data[i].Total), 2);
                  }

              }
          }

      });

  });

