'use strict';

describe('Service: excel', function () {

  // load the service's module
  beforeEach(module('graphMeApp'));

  // instantiate service
  var excel;
  beforeEach(inject(function (_excel_) {
    excel = _excel_;
  }));

  it('should do something', function () {
    expect(!!excel).toBe(true);
  });

});
