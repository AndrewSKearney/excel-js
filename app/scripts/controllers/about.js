'use strict';

/**
 * @ngdoc function
 * @name graphMeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the graphMeApp
 */
angular.module('graphMeApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
