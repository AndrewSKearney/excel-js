'use strict';

describe('Controller: RepChartCtrl', function () {

  // load the controller's module
  beforeEach(module('graphMeApp'));

  var RepChartCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RepChartCtrl = $controller('RepChartCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RepChartCtrl.awesomeThings.length).toBe(3);
  });
});
